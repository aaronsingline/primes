'''
MIT License

Copyright (c) 2017 Aaron Singline

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

from datetime import datetime

print( "Welcome to the Prime Sieve v0.3" )
print( "It is released under the MIT License. Copyright (c) 2017.")

print( "This is based on the Seive of Eratosthenes" )
print
print( "The starting number is always 1.")

start = 1
end = input( "Last number: " )
ouputType = str(raw_input( "[s]creen, [f]ile, [b]oth: " ))

def seive( n ):
	primeStatus = 1
	if n < 2:
		return
	else:
		if n == 2:
			primeStatus = 1
		else:
			for checkFactor in primeNumbers:
				if n % checkFactor == 0 and n > checkFactor:
					primeStatus = 0
					break #breaks out of the loop if a non-prime is detected.
		if primeStatus == 1:
			primeNumbers.append(n)
		return

primeNumbers = [] #set up an empty list.
startTime = datetime.now()
for n in range( start, end ):
	numberprime = seive(n)

if (ouputType == "s") or (ouputType == "b"):
	print primeNumbers

if (ouputType == "f") or (ouputType == "b"):
	outputFile = open('primes.txt', 'w')
	for prime in primeNumbers:
		print>>outputFile, prime
timeTaken = datetime.now() - startTime
print ("Time taken: " + str(timeTaken) )

